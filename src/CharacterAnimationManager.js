var invoke = require('./invoke');
var callIfExists = require('./callIfExists');
var extend = require('./extend');

var CharacterAnimationManager = function(strokes, options) {
  this.strokes = strokes;
  this.options = options;
  this.isRunning = false;
};

CharacterAnimationManager.prototype.run = function() {
  this.strokeNum = this.options.beginAnimationStrokeNum || 0;
  this._runAnimation();
};

CharacterAnimationManager.prototype.stop = function() {
  this.isRunning = false;
  if (this.animationTimer) {
    clearTimeout(this.animationTimer);
  }
  invoke(this.strokes, 'stopStrokeAnimation');
};

CharacterAnimationManager.prototype.updateOptions = function(options) {
  this.options = extend(this.options, options);
};

CharacterAnimationManager.prototype._runAnimation = function() {
  this.isRunning = true;
  invoke(this.strokes.slice(0, this.strokeNum), 'show');
  invoke(this.strokes.slice(this.strokeNum), 'hide');
  this._animateNextStroke();
};

CharacterAnimationManager.prototype._animateNextStroke = function() {
  if (!this.isRunning) return;
  callIfExists(this.options.onBeginStrokeAnimation, { strokeNum: this.strokeNum });

  this.strokes[this.strokeNum].animate(this.options).then(function() {
    callIfExists(this.options.onFinishStrokeAnimation, { strokeNum: this.strokeNum });
    this.strokeNum += 1;
    var isFinished = this.strokeNum >= this.strokes.length;
    if (isFinished && this.options.loopAnimation) {
      this.strokeNum = 0;
      this.animationTimer = setTimeout(this._runAnimation.bind(this), this.options.delayBetweenLoops || 2000);
    } else if (isFinished) {
      this.isRunning = false;
      callIfExists(this.options.onFinishAnimation);
    } else {
      this.animationTimer = setTimeout(this._animateNextStroke.bind(this), this.options.delayBetweenStrokes || 1000);
    }
  }.bind(this));
};

module.exports = CharacterAnimationManager;
